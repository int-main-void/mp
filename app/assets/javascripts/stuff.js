//
//
//
//

function sendpic(data) 
{
   $.ajax({
        type: "POST",
        url: "save",
        data: {
            imgBase64: data//, lat: lat, lon: lon
        }}).done(function(msg) { alert("done saving"); });

}

function saveFormPic() 
{
    var data = $('#cameraInputFile')[0]
    // TODO - get the actual data
    var dataURL="data:image/jpeg;base64,"+btoa(data);
    sendpic(dataURL)
}

function savePicture() {
    var data = $('#imagecanvas')[0].toDataURL("image/jpeg", 0.9);
    var lat = $('#lat')[0];
    var lon = $('#lon')[0];
    sendpic(data)
} 

function retakeClicked() {
    $('#shutterbutton').click(takePicture);
    $('#shutterbutton').prop('value', 'take photo');
    $('#cameradisplay').removeClass('hidden');
    $('#imagecanvas').addClass('hidden');
    $('#save').prop('disabled', true);
    $('#shutterbutton').unbind('click');
    $('#shutterbutton').click(takePicture);
}

//
// called when the 'take picture' button is clicked.
// takes a snapshot from the camera and stores it in the canvas.
//
function takePicture(localMediaStream) {
    if (localMediaStream) {
        var canvas = $('#imagecanvas');
        canvas[0].width = 800;
        canvas[0].height = 500;
        var ctx = canvas[0].getContext('2d');
        var video = $('#cameradisplay')[0];
        
        ctx.drawImage(video, 0, 0);
        // "image/webp" works in Chrome.
        // Other browsers will fall back to image/png.
        // document.querySelector('img').src = canvas.toDataURL('image/webp');

        $('#save').prop('disabled', false);
        $('#cameradisplay').addClass('hidden');
        canvas.removeClass('hidden');
        $('#shutterbutton').prop('value', 'retake');
        $('#shutterbutton').unbind('click');
        $('#shutterbutton').click(retakeClicked);
    }
}

// callback method for setting up camera
function dovid(stream) {
    var localMediaStream = stream;

    $('#shutterbutton').click(takePicture);
    $('#save').click(savePicture);
    var video = document.querySelector('video');
    video.src = window.URL.createObjectURL(localMediaStream);

    // Note: onloadedmetadata doesn't fire in Chrome when using it with getUserMedia.
    // See crbug.com/110938.
    video.onloadedmetadata = function(e) {
        // Ready to go. Do some stuff.
    };
}

function errorCallback(e) {
    console.log('Reeeejected!', e);
}


// callback function to utilize geolocation data
function gotPos(x) { 
    var lat = x.coords.latitude;
    var lon = x.coords.longitude;
    $('#yourpos #lat').text(lat);
    $('#yourpos #lon').text(lon);

}

function errorPos(e) { 
    alert('error getting loc') 
}

function mainfunc() {

    // get current geo location
    navigator.geolocation.getCurrentPosition(gotPos, errorPos,{ enableHighAccuracy:true });

    // set up camera for capturing
    navigator.getUserMedia = (navigator.webkitGetUserMedia ||
                              navigator.mozGetUserMedia ||
                              navigator.msGetUserMedia );

    if (navigator.getUserMedia) {
        var opts = {video: true};
        navigator.getUserMedia(opts, dovid, errorCallback);
    } else {
       // alert('getUserMedia() is not supported in your browser');
        $('#nortmbutton').click(saveFormPic)
        $('#getusermedia').hide();
        $('#camerainputfile').show();
    }

}

$(document).ready(mainfunc);
