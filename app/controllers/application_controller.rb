class ApplicationController < ActionController::Base

  before_filter :authorize, :except => [:login, "welcome/index"]

  #session :session_key => '_mp_session_id'

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected

  def authorize
    unless User.find_by_id(session[:user_id])
      #session[:original_uri] = request.request_uri # TODO
      flash[:notice] = "Please log in"
      redirect_to :controller => :admin, :action => :login
    end
  end


end
