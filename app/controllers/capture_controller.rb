require "base64"

class CaptureController < ApplicationController

  # HACKME turn off CSRF for save since it's currently being used unauthenticated
  skip_before_filter  :verify_authenticity_token, :only => :save


  def index
    @BrandName = "The Brand, Inc."
  end

  #
  # receives image data that is Base64 encoded as a DataURI in a parameter named 'imgBase64'
  #
  def save
    param = params[:imgBase64]
    uri = URI::Data.new(param)
    data = uri.data

    bucket_name = "mediaizer"
    prefix = "/"
    object_key = "media-file-#{Time.now.to_i}.jpg"

    begin
      s3client = get_client
      print s3client
      s3client.put_object(bucket: bucket_name, key: object_key, body: data)
    rescue Aws::S3::Errors::ServiceError => e
      logger.error "error calling AWS S3: #{e}"
    end

    #render :nothing => true

    respond_to do |format|
      format.html
      format.json { render json: '' }
    end

  end

  def list
    begin
      bucket_name = "mediaizer"
      prefix = "/"
      s3client = get_client
      
      thumb_objs = s3client.list_objects({bucket: bucket_name,
                                           prefix: prefix
                                         }).contents

      #thumb_objs.select! { |obj| obj.key =~ /(jpg|gif|png)$/i } # only include images that are jpg, png, or gif
      @stuff = thumb_objs.map { |obj| "http://s3.amazonaws.com/#{bucket_name}/#{obj.key}" } # convert obj to url

    rescue Aws::S3::Errors::ServiceError
      logger.error "error calling AWS S3"
    end 



  end

  private

  def get_client
    aws_region = 'us-west-2' # HACKME
    #aws_region = 'us-east-1'
    #Aws::S3::Client.new(region: aws_region)
    Aws::S3::Client.new(region: aws_region)
  end





end
