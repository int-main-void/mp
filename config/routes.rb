Rails.application.routes.draw do

  get 'admin/login'

  get 'admin/logout'

  get 'admin/index'

  get 'capture' => 'capture#index'

  resources :users
  root 'welcome#index'

  #get ':controller(/:action(:/id))'


end
